#include <iostream>
#include <cmath>
#include <fstream>
#include <string>
#include <sstream>
using namespace std;
#define Min 0.000001

struct monomial
{
	int sobien = 1;
	float coe;
	char *var;
	unsigned short *exp;
	monomial *pNext;
	monomial()
	{}
	monomial(monomial &a)
	{
		coe = a.coe;
		exp = a.exp;
		var = a.var;
		sobien = a.sobien;
	}
	monomial &operator=(monomial a)
	{
		monomial tmp(a);
		tmp.pNext = a.pNext;
		return tmp;
	}
	monomial operator+(monomial a)
	{
		monomial tmp;
		tmp.coe = coe + a.coe;
		tmp.exp = a.exp;
		tmp.var = a.var;
		return tmp;
	}
	monomial operator-(monomial a)
	{
		monomial tmp;
		tmp.coe = coe - a.coe;
		tmp.exp = a.exp;
		tmp.var = a.var;
		return tmp;
	}
	monomial operator*(monomial a)
	{
		monomial tmp = a; // luu a 
		tmp.coe *= coe;
		int m, n;
		m = strlen(a.var);
		n = strlen(var);
		for (int i = 0; i < m; i++)
		{
			for (int j = 0; j < n; j++)
				if (a.var[i] == var[j]) // gap bien giong nhau 
				{
					tmp.exp[i] += exp[j]; // cap nhat mu 
				}
		}
		int count = -1;
		for (int i = 0; i < n; i++)
		{
			bool test = false;
			for (int j = 0; j < m; j++)
				if (var[i] == a.var[j])
					test = true; // bien cua b da ton tai trong don thuc moi 
			if (!test) // khong co bien cua b trong don thuc moi  
			{
				count++;
				tmp.var[m + count] = var[i];
				tmp.exp[m + count] = exp[i];
			}
		}
		return tmp;
	}
};

void Swap(monomial &a, monomial &b);

class Polymial
{
public:
	void shorten();//shorten polynomial by deleting node has coe=0
	void sortPoly();//sorting poly base on exponent and variable(in case exponents equal)
	void sub2Poly(Polymial l1, Polymial l2);//sub 2 Polymials
	void enqueue(monomial data);//enqueue a monomial into polynomial
private:
	monomial *pHead;
	monomial *pTail;
	int compare(monomial T, monomial P);
}

//enqueue a monomial into polynomial
void Polymial::enqueue(monomial data)
{
	monomial *tmp = createMono(data);
	if (pHead == NULL)
	{
		pHead = pTail = tmp;
	}
	else
	{
		pTail->pNext = tmp;
		pTail = tmp;
		pTail->pNext = NULL;
	}
}

//shorten polynomial by deleting node has coe=0
void Polymial::shorten()
{
	if (pHead == NULL)
		return;
	monomial *tmp = pHead;

	while (tmp != NULL)
	{
		if (tmp->coe != 0) // cap nhat he so cho nhung don thuc co bien giong nhau 
		{
			monomial *tmp2 = tmp->pNext;
			while (tmp2 != NULL)
			{
				if (compare(*tmp, *tmp2) == 0) // Da xu li 3 truong hop (a^2*b , b*a^2 ) va ( a^2*b^2 , b^2*a^2) va ( a^2*b^2 , a^2*b^2)
				{
					tmp->coe = tmp->coe + tmp2->coe;
					cout << tmp->coe << endl;
					tmp2->coe = 0;
				}
				tmp2 = tmp2->pNext;
			}
		}
		tmp = tmp->pNext;
	}
	//Delete coe = 0
	tmp = pHead;
	monomial *tmp2 = tmp->pNext;
	if (fabs(tmp->coe) < Min)
	{
		pHead = pHead->pNext;
		delete tmp;
		tmp = pHead;
	}
	while (tmp2 != NULL)
	{
		if (fabs(tmp2->coe) < Min)
		{
			monomial *tmp_del = tmp2;
			tmp2 = tmp2->pNext;
			tmp->pNext = tmp2;
			delete tmp_del;
		}
		else
		{
			tmp = tmp2;
			tmp2 = tmp2->pNext;
		}
	}
}

void Polymial::sortPoly()
{
	monomial *tmp = pHead;
	int mu1, mu2, m, n;
	// sap xep tang dan theo mu , neu mu bang nhau thi so sanh bien 
	while (tmp != NULL) // for ( i = 0 ; i < so luong don thuc ; i ++ ) 
	{
		monomial *tmp2 = tmp->pNext;
		m = strlen(tmp->var); // so luong bien trong don thuc  dang xet 
		mu1 = 0;
		for (int i = 0; i < m; i++)
			mu1 += tmp->exp[i]; // tinh tong mu cua cac bien trong don thuc , A

		while (tmp2 != NULL) // for ( j = i + 1 ; j < so luong don thuc ; j ++ ) 
		{
			n = strlen(tmp2->var);
			mu2 = 0;
			for (int i = 0; i < m; i++)
				mu2 += tmp2->exp[i]; // tinh tong mu cua B

			if ((mu1 > mu2) || (mu1 == mu2 && compare(*tmp, *tmp2) == 1)) // A > B 
			{
				Swap(*tmp, *tmp2); // doi cho 2 node trong da thuc 
			}

			tmp2 = tmp2->pNext;
		}
		tmp = tmp->pNext;
	}
}

//sub 2 polynomials
void Polymial::sub2Poly(Polymial l1, Polymial l2)
{
	monomial *tmp1 = l1.pHead;
	monomial *tmp2 = l2.pHead;
	while (tmp1)
	{
		enqueue(*tmp1);
		*tmp1 = *tmp1->pNext;
	}
	while (tmp2 != NULL)
	{
		tmp2->coe *= -1;
		enqueue(*tmp2);
		*tmp2 = *tmp2->pNext;
	}
	shorten();
	sortPoly();
}